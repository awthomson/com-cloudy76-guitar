const margin_x = 100;
const margin_y = 40;
var pos_markers = [];

const fret_marker_size = 20;
const fret_size = 5;
var num_frets = 15;
var tuning = ["E","B","G", "D", "A", "E"];

var selected_notes = [];
var root_note = "";

window.addEventListener('resize', init_screen);

parse_params();
init_screen();
init_metronome();

function init_screen() {
	console.log("Init screen");
	create_markers();
	add_markers();
	draw_fretboard();
	redraw_markers();
	update_notes_list();
}

function generate_url() {
	var URL="http://localhost/?";
	var t = url_adjust(tuning.reverse().toString());
	var n = url_adjust(selected_notes.sort().toString());
	URL += "tuning="+t;
	if (n) {
		URL += "&notes="+n;
	}
	if (root_note) {
		URL += "&root="+url_adjust(root_note);
	}
	var frets = document.getElementById("num_frets").value;
	URL += "&frets="+frets;
        url_link = document.getElementById("url");
	url_link.value=URL;
}
function url_adjust(s) {
	s=s.replace(/A#/gi, "Bb");
	s=s.replace(/C#/gi, "Db");
	s=s.replace(/D#/gi, "Eb");
	s=s.replace(/F#/gi, "Gb");
	s=s.replace(/G#/gi, "Ab");
	return s;
}

function parse_params() {
	console.log("Parsing parameters");
	const queryString = window.location.search;
	const urlParams = new URLSearchParams(queryString);
	const notes = urlParams.get('notes')
	const this_tuning = urlParams.get('tuning')
	const root = urlParams.get('root')
	const frets = urlParams.get('frets')
	if (notes) selected_notes = parse_notes(notes);
	if (this_tuning) tuning = parse_tuning(this_tuning);
	if (root) root_note = parse_root(root);
	if (frets) {
		num_frets = parseInt(frets);
		if (num_frets < 5 || num_frets > 20)
			num_frets = 15;

	}
	document.getElementById("num_frets").value = num_frets;
}

// Helper function to convert arrays of flat notes (URLs can't use #) into sharps 
function parse_notes(n) {
	var dest = [];
	a = n.toUpperCase().split(',').sort();
	for (var i=0; i<a.length; i++) {
		a[i].toUpperCase;
		if (a[i] == "AB") a[i]="G#";	
		if (a[i] == "BB") a[i]="A#";	
		if (a[i] == "DB") a[i]="C#";	
		if (a[i] == "EB") a[i]="D#";	
		if (a[i] == "GB") a[i]="F#";
		dest.push(a[i]);
	}
	return dest;
}

// Helper function to convert arrays of flat notes (URLs can't use #) into sharps
function parse_tuning(n) {
        var dest = [];
        a = n.toUpperCase().split(',').reverse();
        for (var i=0; i<a.length; i++) {
                a[i].toUpperCase;
                if (a[i] == "AB") a[i]="G#";
                if (a[i] == "BB") a[i]="A#";
                if (a[i] == "DB") a[i]="C#";
                if (a[i] == "EB") a[i]="D#";
                if (a[i] == "GB") a[i]="F#";
                dest.push(a[i]);
        }
        return dest;
}

// Helper function to convert arrays of flat notes (URLs can't use #) into sharps
function parse_root(n) {
        a = n.toUpperCase();
        if (a == "AB") a="G#";
        if (a == "BB") a="A#";
        if (a == "DB") a="C#";
        if (a == "EB") a="D#";
        if (a == "GB") a="F#";
        return a;
}




/*
 * Metronome related functions
 * 									*/
var audio;

function init_metronome() {
	const metronomeAudioCtx = new AudioContext();
	audio = new Audio("tick.wav");
	const source = metronomeAudioCtx.createMediaElementSource(audio);
	source.connect(metronomeAudioCtx.destination);
	
}

function change_bpm() {
	// Stop the mtronome
	btn.innerText = "Play";
	btn.classList.remove("btn_running");
	clearInterval(my_metronome);
}

function toggle_metronome() {
        btn = document.getElementById("btn_metronome");
        if (btn.classList.contains('btn_running')) {
                // Stop the mtronome
                btn.innerText = "Play";
                btn.classList.remove("btn_running");
		clearInterval(my_metronome);
        } else {
                // Start the metronome
                btn.innerText = "Stop";
                btn.classList.add("btn_running");
        	bpm = document.getElementById("num_bpm").value;
	        my_metronome = setInterval(function(){ 
			tick();
	        }, 60000/bpm);
        }
}

var beat = 0;
function tick() {
	setTimeout(function(){
		audio.play();
		beat++;
		if (beat == 4) beat = 0;
	},100);
}

/*
 * Tuner related functions
 * 									*/
var osc;

function toggle_440() {
	btn = document.getElementById("btn_440");	
	if (btn.classList.contains('btn_running')) {
		// Stop the tuner
		btn.innerText = "Play";
		btn.classList.remove("btn_running");
		osc.stop(0);
	} else {
		// Start the tuner
		btn.innerText = "Stop";
		btn.classList.add("btn_running");
        	var context=new AudioContext();
        	osc = context.createOscillator();
        	osc.type = "sine";
        	osc.frequency.value = 440;
        	osc.connect(context.destination);
		osc.start(0);
	}
}

function select_tuning() {
	sel_tuning = document.getElementById("tuning").value;
	console.log("Selected: "+sel_tuning);
	if (sel_tuning == "flat6") tuning = ["D#", "A#", "F#", "C#", "G#", "D#"];
	if (sel_tuning == "flat4") tuning = ["F#", "C#", "G#", "D#"];
	if (sel_tuning == "d6") tuning = ["E", "B", "G", "D", "A", "D"];
	if (sel_tuning == "d4") tuning = ["G", "D", "A", "D"];
	if (sel_tuning == "openc6") tuning = ["E", "C", "G", "C", "G", "C"];
	if (sel_tuning == "opend6") tuning = ["D", "A", "F#", "D", "A", "D"];
	if (sel_tuning == "opene6") tuning = ["E", "B", "G#", "E", "B", "E"];
	if (sel_tuning == "openf6") tuning = ["F", "A", "F", "C", "F", "C"];
	if (sel_tuning == "openg6") tuning = ["D", "B", "G", "D", "G", "D"];
	if (sel_tuning == "s7") tuning = ["E", "B", "G", "D", "A", "E", "B"];
	if (sel_tuning == "s6") tuning = ["E", "B", "G", "D", "A", "E"];
	if (sel_tuning == "s5") tuning = ["G", "D", "A", "E", "B"];
	if (sel_tuning == "s4") tuning = ["G", "D", "A", "E"];
	create_markers();
        add_markers();
        draw_fretboard();
        redraw_markers();
        update_notes_list();
}
function change_number_of_frets() {
	num_frets = parseInt(document.getElementById("num_frets").value);
	create_markers();
	add_markers();
	draw_fretboard();
	redraw_markers();
	update_notes_list();
}

function create_markers() {
	const num_strings = tuning.length;
	var c = document.getElementById("fretboard_canvas");
	var bounding_rect = c.getBoundingClientRect();
	var off_x = bounding_rect.left;

	const w = c.scrollWidth;
	const h = c.scrollHeight;
	const fret_dist = (w-(margin_x*2))/(num_frets+0.5);
        const string_dist = (h-(margin_y*2))/num_strings;

	pos_markers = [];
	for (var s=0; s<num_strings; s++) {
		n = tuning[s];
		for (f=0; f<=num_frets; f++) {
			m = {"note":n, "x":(f*fret_dist)+margin_x+off_x-30, "y":(s*string_dist)+margin_y+10};
			pos_markers.push(m);
			n = next_note(n)
		}
	}
}

function next_note(n) {
	if (n == "C") return "C#";
	if (n == "C#") return "D";
	if (n == "D") return "D#";
	if (n == "D#") return "E";
	if (n == "E") return "F";
	if (n == "F") return "F#";
	if (n == "F#") return "G";
	if (n == "G") return "G#";
	if (n == "G#") return "A";
	if (n == "A") return "A#";
	if (n == "A#") return "B";
	if (n == "B") return "C";
	return "ERROR";
}

function draw_fretboard() {

	var num_strings = tuning.length;

	var c = document.getElementById("fretboard_canvas");
	w = c.scrollWidth;
	h = c.scrollHeight;
	var ctx = c.getContext("2d");

	// Clear canvas
	ctx.fillStyle = "#eeeeee";
	ctx.fillRect(0, 0, w, h);


	// Draw fretboard
	ctx.fillStyle = "#600000";
	ctx.fillRect(margin_x, margin_y, w-(margin_x*2), h-(margin_y*2));

	// Draw frets
	const fret_dist = (w-(margin_x*2))/(num_frets+0.5)

	ctx.fillStyle = "#aaaaaa";
	for (var i=0; i<=num_frets; i++) {
		ctx.fillRect(i*fret_dist+margin_x, margin_y, fret_size, h-(margin_y*2));
	}

	// Draw fret markers
	// 3rd fret
	ctx.beginPath();
	ctx.arc(2.5*fret_dist+margin_x+4, h/2, fret_marker_size, 0, 2*Math.PI, false);
	ctx.fillStyle = '#888888';
	ctx.fill();
	ctx.font = "30px Roboto";
	ctx.fillStyle = "#000000";
	var txt = "3rd";
	var tw = ctx.measureText(txt).width/2;
	ctx.fillText(txt, 2.5*fret_dist+margin_x+4-tw, 30); 

	// 5th fret
	ctx.beginPath();
	ctx.arc(4.5*fret_dist+margin_x+4, h/2, fret_marker_size, 0, 2*Math.PI, false);
	ctx.fillStyle = "#888888";
	ctx.fill();
	ctx.font = "30px Roboto";
	ctx.fillStyle = "#000000";
	var txt = "5th";
	var tw = ctx.measureText(txt).width/2;
	ctx.fillText(txt, 4.5*fret_dist+margin_x+4-tw, 30); 

	// 7th fret
	if (num_frets  >= 7) {
		ctx.beginPath();
		ctx.arc(6.5*fret_dist+margin_x+4, h/2, fret_marker_size, 0, 2*Math.PI, false);
		ctx.fillStyle = '#888888';
		ctx.fill();
		ctx.font = "30px Roboto";
		ctx.fillStyle = "#000000";
		var txt = "7th";
		var tw = ctx.measureText(txt).width/2;
		ctx.fillText(txt, 6.5*fret_dist+margin_x+4-tw, 30); 
	}

	// 9th fret
	if (num_frets  >= 9) {
		ctx.beginPath();
		ctx.arc(8.5*fret_dist+margin_x+4, h/2, fret_marker_size, 0, 2*Math.PI, false);
		ctx.fillStyle = '#888888';
		ctx.fill();
		ctx.font = "30px Roboto";
		ctx.fillStyle = "#000000";
		var txt = "9th";
		var tw = ctx.measureText(txt).width/2;
		ctx.fillText("9th", 8.5*fret_dist+margin_x+4-tw, 30); 
	}

	// 12th fret
	if (num_frets  >= 12) {
		ctx.beginPath();
		ctx.arc(11.5*fret_dist+margin_x+4, h*0.3333, fret_marker_size, 0, 2*Math.PI, false);
		ctx.fillStyle = '#888888';
		ctx.fill();
		ctx.beginPath();
		ctx.arc(11.5*fret_dist+margin_x+4, h*0.6666, fret_marker_size, 0, 2*Math.PI, false);
		ctx.fillStyle = '#888888';
		ctx.fill();
		ctx.font = "30px Roboto";
		ctx.fillStyle = "#000000";
		var txt = "12th";
		var tw = ctx.measureText(txt).width/2;
		ctx.fillText(txt, 11.5*fret_dist+margin_x+4-tw, 30); 
	}

	// 15th fret
	if (num_frets  >= 15) {
		ctx.beginPath();
		ctx.arc(14.5*fret_dist+margin_x+4, h/2, fret_marker_size, 0, 2*Math.PI, false);
		ctx.fillStyle = '#888888';
		ctx.fill();
		ctx.font = "30px Roboto";
		ctx.fillStyle = "#000000";
		var txt = "15th";
		var tw = ctx.measureText(txt).width/2;
		ctx.fillText(txt, 14.5*fret_dist+margin_x+4-tw, 30); 
	}

	if (num_frets  >= 17) {
		ctx.beginPath();
		ctx.arc(16.5*fret_dist+margin_x+4, h/2, fret_marker_size, 0, 2*Math.PI, false);
		ctx.fillStyle = '#888888';
		ctx.fill();
		ctx.font = "30px Roboto";
		ctx.fillStyle = "#000000";
		var txt = "17th";
		var tw = ctx.measureText(txt).width/2;
		ctx.fillText(txt, 16.5*fret_dist+margin_x+4-tw, 30); 
	}

	if (num_frets  >= 19) {
		ctx.beginPath();
		ctx.arc(18.5*fret_dist+margin_x+4, h/2, fret_marker_size, 0, 2*Math.PI, false);
		ctx.fillStyle = '#888888';
		ctx.fill();
		ctx.font = "30px Roboto";
		ctx.fillStyle = "#000000";
		var txt = "19th";
		var tw = ctx.measureText(txt).width/2;
		ctx.fillText(txt, 18.5*fret_dist+margin_x+4-tw, 30); 
	}

	// Draw nut
	ctx.fillStyle = '#aaaaaa';
	ctx.fillRect(margin_x-5, margin_y, 7, h-(margin_y*2));

	// Draw Strings
	const string_dist = (h-(margin_y*2))/num_strings;
	for (var i=0; i<num_strings; i++) {
		ctx.fillStyle = "#ffffaa";
		ctx.fillRect(margin_x-5, i*string_dist+margin_y+(string_dist/2), w-(margin_x*2), (i/2)+1);
		ctx.font = "30px Roboto";
		ctx.fillStyle = "#000000";
		ctx.fillText(tuning[i], margin_x-50, i*string_dist+margin_y+(string_dist/2)+13); 
	}
	
	
}

/*
 * 	Create the note markers in the DOM
 * 								*/
function add_markers() {
        var frets = document.getElementById("frets");
	while (frets.firstChild) {
    		frets.removeChild(frets.lastChild);
  	}
	frets.innerHTML = "";
        for (var i = 0; i < pos_markers.length; i++) {
		var tag = document.createElement("p");
               	tag.textContent=pos_markers[i].note;
               	tag.classList.add("marker");
               	tag.classList.add(pos_markers[i].note);
		tag.addEventListener("click", select_note);
               	tag.style.left = pos_markers[i].x-12;
               	tag.style.top = pos_markers[i].y;
		frets.appendChild(tag);
        }
}

/*
 * User has clicked on a note marker
 *					*/
function select_note() {
	var sel_classes = this.className;
	var sel_note = "";
	if (sel_classes.includes("C")) sel_note = "C";
	if (sel_classes.includes("C#")) sel_note = "C#";
	if (sel_classes.includes("D")) sel_note = "D";
	if (sel_classes.includes("D#")) sel_note = "D#";
	if (sel_classes.includes("E")) sel_note = "E";
	if (sel_classes.includes("F")) sel_note = "F";
	if (sel_classes.includes("F#")) sel_note = "F#";
	if (sel_classes.includes("G")) sel_note = "G";
	if (sel_classes.includes("G#")) sel_note = "G#";
	if (sel_classes.includes("A")) sel_note = "A";
	if (sel_classes.includes("A#")) sel_note = "A#";
	if (sel_classes.includes("B")) sel_note = "B";

	if (selected_notes.includes(sel_note)) {
                if (root_note == sel_note) {
                        // Unselect note
                        var filtered = selected_notes.filter(function(value, index, arr) {
                                return value != sel_note;
                        });
                        selected_notes = filtered;
			root_note = "";
		} else {
			// Make it the root note
			root_note = sel_note;
		}
	} else {
		// Select note
		selected_notes.push(sel_note);
	}
	redraw_markers();
	update_notes_list();
}


function clear_notes() {
	selected_notes = [];
	redraw_markers();
	update_notes_list();
}

function redraw_markers() {

	// Mark them all inactive
	const collection = document.getElementsByClassName("marker");	
	for (var i=0; i<collection.length; i++) {
		collection[i].classList.remove("sel");
		collection[i].classList.remove("root");
	}

	// Mark those selected as active
	for (var i=0; i<selected_notes.length; i++) {
		const collection = document.getElementsByClassName(selected_notes[i]);
		for (var c=0; c<collection.length; c++) {
			collection[c].classList.add("sel");
			if (selected_notes[i] == root_note) {
				collection[c].classList.add("root");
			}
		}
	}
}

/*
 * 	Show information like which chords, modes, etc. apply
 * 								*/
function update_notes_list() {
        var nl = document.getElementById("notes");
	nl.innerHTML = "";
        for (var i=0; i<selected_notes.length; i++) {
                var note = document.createElement("div");
                note.textContent = selected_notes[i];
		note.classList.add('item');
                nl.appendChild(note);
        }

	var found_chords = check_chords();
        var cl = document.getElementById("chords");
	cl.innerHTML='';
	for (var i=0; i<found_chords.length; i++) {
		var c = document.createElement("div");
		c.textContent = found_chords[i];
		c.classList.add('item2');
		cl.appendChild(c);
	}

        var found_modes = check_modes();
        var ml = document.getElementById("modes");
        ml.innerHTML='';
	if (found_modes == null)
		return;
        for (var i=0; i<found_modes.length; i++) {
                var m = document.createElement("div");
                m.textContent = found_modes[i];
                m.classList.add('item4');
                ml.appendChild(m);
        }

}

/*
 * 	Given the selected notes, find which chords apply
 * 								*/
function check_chords() {
	var found_chords = [];
	for (var i=0; i<chords.length; i++) {
		cn = chords[i].notes;
		var found = 1;
		for (j=0; j<cn.length; j++) {
			if (!selected_notes.includes(cn[j])) {
				found = 0;
			}
		}
		if (found == 1) {
			found_chords.push(chords[i].name);
		}
	}
	return found_chords;
}

/*
 *      Given the selected notes, find which chords apply
 *                                                              */
function check_modes() {

	// Do'nt bother if there's only a couple of notes selected
	if (selected_notes.length < 3) return;

        var found_modes= [];
        for (var i=0; i<modes.length; i++) {
                var mn = modes[i].notes;
                var found = 1;
                for (j=0; j<selected_notes.length; j++) {
                        if (!mn.includes(selected_notes[j])) {
                                found = 0;
                        }
                }
                if (found == 1) {
                        found_modes.push(modes[i].name);
                }
        }
        return found_modes;
}

function select_tab(id) {
	const tabs = document.getElementById("tabs").children[0];
	const c = tabs.children;
	for (i=0; i<c.length; i++) {
		c[i].classList.remove("active");
	}
	id.classList.add("active");
	if (id.innerText == "Info") 
		select("info");
	if (id.innerText == "Tuner") 
		select("tuner");
	if (id.innerText == "Metronome") 
		select("metronome");
	if (id.innerText == "Backing") 
		select("backing");
	if (id.innerText == "Settings") 
		select("settings");
	if (id.innerText == "Modes") 
		select("modes");
	if (id.innerText == "About") 
		select("about");
	if (id.innerText == "Misc") 
		select("misc");

}

function select(page) {
        const pages = document.getElementById("pages");
        const c = pages.children;
        for (i=0; i<c.length; i++) {
                c[i].classList.remove("active");
        }
	document.getElementById(page+"_group").classList.add("active");
}

function select_mode() {
        const root = document.getElementById("root").value;
        const mode = document.getElementById("mode").value;
	for (var i=0; i<modes.length; i++) {
		if (modes[i].name.includes(mode)) {
			if (modes[i].root == root) {
				selected_notes = modes[i].notes;
				root_note = root;
				init_screen();
			}
		}

	}
}
